﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Simple_Diff
{
    class Program
    {
        static void Main(string[] args)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;

            while (true)
            {
                Console.WriteLine("Before");
                string before = Console.ReadLine();
                Console.WriteLine("After");
                string after = Console.ReadLine();

                before = before.Replace("\"", "");
                after = after.Replace("\"", "");

                Console.WriteLine($"{before}\n{after}");
               

                ExcelPackage before_excel = new ExcelPackage(new FileInfo(before));
                ExcelPackage after_excel = new ExcelPackage(new FileInfo(after));

                foreach (ExcelWorksheet before_ws in before_excel.Workbook.Worksheets)
                {
                    ExcelWorksheet after_ws = after_excel.Workbook.Worksheets[before_ws.Name];
                    for (int row = 1; row < after_ws.Dimension.Rows + 1; row++)
                    {
                        for (int col = 1; col < after_ws.Dimension.Columns + 1; col++)
                        {
                            if (before_ws.Cells[row, col].Value != null)
                            {



                                if (after_ws.Cells[row, col].Value != null)
                                {
                                    if (before_ws.Cells[row, col].Value.ToString() != after_ws.Cells[row, col].Value.ToString())
                                    {
                                        Console.WriteLine(before_ws.Cells[row, col].Value.ToString());
                                        after_ws.Cells[row, col].Style.Fill.SetBackground(ColorTranslator.FromHtml("#ffff00"), OfficeOpenXml.Style.ExcelFillStyle.Solid);
                                    }
                                }
                                else
                                {
                                    after_ws.Cells[row, col].Style.Fill.SetBackground(ColorTranslator.FromHtml("#ffff00"), OfficeOpenXml.Style.ExcelFillStyle.Solid);
                                }

                            }
                            else
                            {

                                if (after_ws.Cells[row, col].Value != null)
                                {
                                    if (after_ws.Cells[row, col].Value.ToString() != "")
                                    {
                                        after_ws.Cells[row, col].Style.Fill.SetBackground(ColorTranslator.FromHtml("#ffff00"), OfficeOpenXml.Style.ExcelFillStyle.Solid);
                                    }

                                }

                            }
                        }


                    }
                }
                

                after_excel.SaveAs(new FileInfo(after.Replace(".xls", "_changes.xls")));
                Console.WriteLine("Done");
            }

            

        }
    }
}
